const {
    mix
} = require('laravel-mix');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.autoload({
    jquery: ['$', 'window.jQuery', 'jQuery'],
    particleJS: ['particleJS']
});

mix.
js([
        'resources/assets/js/bootstrap.min.js',
        /*'node_modules/particles.js/particles.js',*/
        'node_modules/jquery-waypoints/waypoints.js',
        'node_modules/owl.carousel/dist/owl.carousel.js',
        'node_modules/jquery.scrollTo/jquery.scrollTo.min.js',
        'node_modules/jquery.localScroll/jquery.localScroll.min.js',
        /*'resources/assets/js/owl.carousel.min.js',*/
        'node_modules/bootstrap-sweetalert/dist/sweetalert.min.js',
        'resources/assets/js/particle.js',
        'resources/assets/js/jquery.sticky.js',
        'resources/assets/js/site.js',
    ],
    'public/js/app.js').
sass('resources/assets/sass/app.scss', 'public/css');

mix.browserSync({
    host: 'pcv.dev',
    proxy: 'http://pcv.dev',
    port: 3200
});
