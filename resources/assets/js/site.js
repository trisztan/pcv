global.$ = global.jQuery = require('jquery');

(function ($) {
    'use strict';

    $(document).ready(function () {

        particlesJS("particles-js", {
            "particles": {
                "number": {
                    "value": 33,
                    "density": {
                        "enable": true,
                        "value_area": 394.57382081613633
                    }
                },
                "color": {
                    "value": "#ffffff"
                },
                "shape": {
                    "type": "edge",
                    "stroke": {
                        "width": 0,
                        "color": "#000000"
                    },
                    "polygon": {
                        "nb_sides": 5
                    },
                    "image": {
                        "src": "img/github.svg",
                        "width": 100,
                        "height": 100
                    }
                },
                "opacity": {
                    "value": 0.15232414578222467,
                    "random": false,
                    "anim": {
                        "enable": false,
                        "speed": 1,
                        "opacity_min": 0.1,
                        "sync": false
                    }
                },
                "size": {
                    "value": 8.017060304327615,
                    "random": true,
                    "anim": {
                        "enable": false,
                        "speed": 40,
                        "size_min": 0.1,
                        "sync": false
                    }
                },
                "line_linked": {
                    "enable": true,
                    "distance": 150,
                    "color": "#ffffff",
                    "opacity": 0.4,
                    "width": 1
                },
                "move": {
                    "enable": true,
                    "speed": 6,
                    "direction": "none",
                    "random": true,
                    "straight": false,
                    "out_mode": "out",
                    "bounce": false,
                    "attract": {
                        "enable": false,
                        "rotateX": 600,
                        "rotateY": 1200
                    }
                }
            },
            "interactivity": {
                "detect_on": "canvas",
                "events": {
                    "onhover": {
                        "enable": true,
                        "mode": "grab"
                    },
                    "onclick": {
                        "enable": true,
                        "mode": "repulse"
                    },
                    "resize": true
                },
                "modes": {
                    "grab": {
                        "distance": 400,
                        "line_linked": {
                            "opacity": 1
                        }
                    },
                    "bubble": {
                        "distance": 400,
                        "size": 40,
                        "duration": 2,
                        "opacity": 8,
                        "speed": 3
                    },
                    "repulse": {
                        "distance": 200,
                        "duration": 0.4
                    },
                    "push": {
                        "particles_nb": 4
                    },
                    "remove": {
                        "particles_nb": 2
                    }
                }
            },
            "retina_detect": true
        });

        var window_width = $(window).width();

        if ($.fn.waypoint) {
            $('.animated').css('opacity', '0');
            $('.animated').waypoint(function () {
                $(this).addClass('fadeInUp');
                $('.animated.fadeInUp').css({
                    opacity: 1
                });
            }, {
                offset: '90%'
            });
        }

        if ($.fn.owlCarousel) {
            var hero_slider = $('.hero_slider');
            hero_slider.owlCarousel({
                items: 1,
                loop: true,
                autoplay: true,
                autoplayTimeout: 7000,
                margin: 30,
                nav: true,
                navText: ['<i class="icofont icofont-simple-left"></i>', '<i class="icofont icofont-simple-right"></i>']
            });


            hero_slider.on('translate.owl.carousel', function () {
                $(this).find('.homeImg').removeClass('fadeInLeft animated').css('opacity', '0');
                $(this).find('.homeSlider').removeClass('fadeInRight animated').css('opacity', '0');
            });
            hero_slider.on('translated.owl.carousel', function () {
                $(this).find('.homeImg').addClass('fadeInLeft animated').css('opacity', '1');
                $(this).find('.homeSlider').addClass('fadeInRight animated').css('opacity', '1');
            });

            var arrowPos;
            if (window_width > 1199) {
                var arrowPos = (window_width - 1140) / 2;
            } else if (window_width > 991 && window_width < 1200) {
                var arrowPos = (window_width - 940) / 2;
            } else if (window_width > 767 && window_width < 992) {
                var arrowPos = (window_width - 720) / 2;
            } else {
                $('.hero_slider .owl-nav div').remove();
            }

            $('.hero_slider .owl-nav div.owl-next').css('right', '-' + arrowPos + 'px');
            $('.hero_slider .owl-nav div.owl-prev').css('left', '-' + arrowPos + 'px');




        }

        if ($.fn.owlCarousel) {
            var tstSlider2 = $('.tstSlider2');
            tstSlider2.owlCarousel({
                items: 3,
                loop: true,
                autoplay: true,
                autoplayTimeout: 7000,
                margin: 30,
                responsive: {
                    0: {
                        items: 1,
                        margin: 0
                    },
                    768: {
                        items: 2
                    },
                    992: {
                        items: 3
                    }
                }
            });
        }

        if ($.fn.owlCarousel) {
            //tstSlider
            var tstSlider = $('.tstSlider');
            tstSlider.owlCarousel({
                items: 1,
                dots: true,
                loop: true,
                autoplay: true,
                autoplayTimeout: 7000
            });

            tstSlider.on('translate.owl.carousel', function () {
                $(this).find('.owl-item .singleSlide .tstTxt p').removeClass('fadeInLeft animated').css('opacity', '0');
                $(this).find('.owl-item .singleSlide .tstTxt > div').removeClass('fadeInUp animated').css('opacity', '0');
                $(this).find('.owl-item .singleSlide .tstImg').removeClass('zoomInRight animated').css('opacity', '0');
            });
            tstSlider.on('translated.owl.carousel', function () {
                $(this).find('.owl-item.active .singleSlide .tstTxt p').addClass('fadeInLeft animated').css('opacity', '1');
                $(this).find('.owl-item.active .singleSlide .tstTxt > div').addClass('fadeInUp animated').css('opacity', '1');
                $(this).find('.owl-item.active .singleSlide .tstImg').addClass('zoomInRight animated').css('opacity', '1');
            });
        }

        if ($.fn.owlCarousel) {
            //brandSlider
            var brandSlider = $('.brandSlider');
            brandSlider.owlCarousel({
                items: 5,
                loop: true,
                dots: false,
                nav: false,
                margin: 50,
                autoplay: true,
                smartSpeed: 600,
                autoplayTimeout: 3000,
                responsive: {
                    0: {
                        items: 3,
                        margin: 20
                    },
                    480: {
                        items: 4,
                        margin: 30
                    },
                    768: {
                        items: 5,
                        margin: 50
                    }
                }
            });
        } else {
            alert('not defined');
        }

        //searchForm
        /*$('.cartSearch li.search a').on("click", function () {
            $('.searchForm').toggleClass('active');
        });
        $('.searchForm i').on("click", function () {
            $('.searchForm').toggleClass('active');
        });*/
        $(document).on('click','.dropdown-cart', function($) {
            $.stopPropagation();
        });

        //disable blank anchor tag
        $("a[href='#']").on("click", function ($) {
            $.preventDefault();
        });

        //add to cart
        $('.addCart').on("click", function () {
            $(this).toggleClass('clicked');
        });

        // submenu parent add class
        /*$('.dropdown-menu').each(function () {
            $(this).closest('li').addClass('dropdown');
        });
        $('.mega-menu').each(function () {
            $(this).closest('li').addClass('static');
        });*/


        if (window_width < 992) {
            $('.dropdown a').on('click', function () {
                $(this).siblings('ul').slideToggle();
                $(this).toggleClass('active');
                $(this).closest('li').toggleClass('mb-none');

            });
        }


        $('.innerCart li.duration .fa-caret-left').on("click", function () {
            var num = parseInt($(this).siblings('span').text());
            var num = num - 1;
            if (num == 0) {
                return;
            }
            $(this).siblings('span').text(num);

        });
        $('.innerCart li.duration .fa-caret-right').on("click", function () {
            var num = parseInt($(this).siblings('span').text());
            var num = num + 1;
            $(this).siblings('span').text(num);
        });

        $('.singleCart > i').on("click", function () {
            $(this).parent().fadeOut(function () {
                $(this).remove();
            });
        });


        $('.layoutBg .singleBg').each(function () {
            var bgImg = $(this).find('img').attr('src');
            $(this).css('background-image', 'url(' + bgImg + ')');
            $(this).on("click", function () {
                $('body').css('background-image', 'url(' + bgImg + ')')
            });
        });

        $('.layoutBtn .boxed').on("click", function () {
            $('.mainWrap').addClass('active');
            $('.layoutBtn a').removeClass('active');
            $(this).addClass('active');
            $('.layout_bg_wrap').slideDown();
        });
        $('.layoutBtn .wide').on("click", function () {
            $('.mainWrap').removeClass('active');
            $('.layoutBtn a').removeClass('active');
            $(this).addClass('active');
            $('.layout_bg_wrap').slideUp();
        });

        $('.colorTheme .singleTheme').each(function () {
            var linkHref = $(this).find('span').text();
            $(this).on("click", function () {
                $('.theme-link link').attr('href', linkHref);


                $('.colorTheme .singleTheme').removeClass('active');
                $(this).addClass('active');
            });
        });

        $('.styler .icon').on("click", function () {
            $('.styler').toggleClass('active');
        });



        /*if (window_width > 991) {
            $('.dropdown').on('mouseenter', function () {
                $(this).addClass('open');
            });
            $('.dropdown').on('mouseleave', function () {
                $(this).removeClass('open');
            });
        }*/

    });

    var color_x = ($('.v2').length) ? "#288feb" : "#ffffff";


})(jQuery);

var App = function () {
    'use strict';

    var verifyToken = function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    }
    var stickyHeader = function () {
        $(".headerBtm").sticky({
            topSpacing: 0,
            zIndex: '2147483647'
        });
    }

    var cartAdd = function () {

        $(document).on("click", "[data-cart-add]", function () {

            var item = $(this).attr('data-cart-add');
            var id = $(this).attr('data-cart-add-id');
            var type = $(this).attr('data-cart-add-type');

            $.ajax({
                url: URL_CARTADD,
                method: "POST",
                dataType: "json",
                data: {
                    "id": id,
                    "item": item,
                    "type": type,
                    "year": 1,
                }
            }).success(function (result) {

                if (result.success) {
                    $('.nav-cart').replaceWith(result.html);
                    $('[data-toggle-cart]').click();
                    swal({
                            title: "Siker!",
                            text: "A " + item + " kosaradba került.",
                            type: "success",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Kosár megnyitása",
                            cancelButtonText: "Tovább vásárlok",
                            closeOnConfirm: false
                        },
                        function () {
                            window.location.replace(URL_CHECKOUT);
                        });
                } else {

                }
            });

        });
    }

    var cartRemove = function () {
        $(document).on("click", "[data-cart-remove]", function () {
            var id = $(this).attr('data-cart-remove');

            $.ajax({
                url: URL_CARTREMOVE,
                method: "POST",
                dataType: "json",
                data: {
                    "id": id,
                }
            }).success(function (result) {
                if (result.success) {
                    $('.nav-cart').replaceWith(result.base_cart);
                    $('.smallCartTotalWrep').replaceWith(result.checkout_cart);
                } else {
                    window.location.reload();
                }
            });
        });
    }

    var domainSearch = function () {
        $(document).on("click", "[data-domain-search]", function () {
            var button = $(this);
            var address = $('[data-domain-address]');

            if (address.val()) {
                $.ajax({
                    url: URL_DOMAINSEARCH,
                    method: "POST",
                    dataType: "json",
                    data: {
                        "domain": address.val(),
                    }
                }).success(function (result) {

                    $('[data-domain-show]').removeClass('alert-success alert-danger');

                    if (result.success) {
                        $('[data-domain-show]').addClass('alert-success').html('<i class="fa fa-check"></i><strong> ' + result.success + '</strong><span class="alert-button"><button data-cart-add="' + address.val() + '" data-cart-add-type="domain" type="button" class="btn btn-blue animated infinite pulse"><i class="fa fa-shopping-cart"></i> Kosárba</button></span>');
                    } else {
                        $('[data-domain-show]').addClass('alert-danger').html('<i class="fa fa-times"></i><strong> ' + result.error + '</strong>');
                    }
                    $('[data-domain-show]').removeClass('hide');
                });
            } else {
                $('[data-domain-show]').addClass('alert-danger').html('<i class="fa fa-times"></i><strong> A domain cím megadása kötelező</strong> ');
                $('[data-domain-show]').removeClass('hide');
            }
        });
    }

    var dinamicAjax = function () {
        function dynamic_modal(type, id) {
            $.ajax({
                url: $("[data-list-modal-url]").val(),
                method: "POST",
                dataType: "html",
                data: {
                    "type": type,
                    "id": id
                }
            }).success(function (result) {
                result = JSON.parse(result);
                $('[data-modal]').empty();
                $('[data-modal]').html(result.data);
                $('#listmodal').modal('show');
            });
        }
    }

    var dashboard = function () {
        $("div.bhoechie-tab-menu>div.list-group>a").click(function (e) {
            e.preventDefault();
            $(this).siblings('a.active').removeClass("active");
            $(this).addClass("active");
            var index = $(this).index();
            $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
            $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
        });
    }

    var scroll = function () {
        $('#navbar').bind('click', 'ul li a', function (event) {
            $.scrollTo(event.target.hash, 1000, {
                offset: -60
            });
        });
    }

    var clientLogin = function () {
        // @FIXME Login modal for checkout page
    }

    return {
        init: function () {
            verifyToken();
            scroll();
            clientLogin();
            cartAdd();
            cartRemove();
            stickyHeader();
            domainSearch();
            dashboard();
        }

    };

}();

$(function () {
    App.init();
});