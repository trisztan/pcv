var Home = function () {
    'use strict';
    console.log(URL_BASE);
    var domainSearch = function () {
        $(document).on("click", "[data-domain-search]", function () {
            var button = $(this);
            var address = $('[data-domain-address]');

            if (address.val()) {

                $.ajax({
                    url: URL_DOMAINSEARCH,
                    method: "POST",
                    dataType: "html",
                    data: {
                        "domain": address.val(),
                    }
                }).success(function (result) {
                    console.log(result);
                });
            } else {
                address.addClass('border-3-red');
                address.attr('placeholder', 'A domain cím megadása kötelező');
            }
        });
    }

    var dinamicAjax = function () {
        function dynamic_modal(type, id) {
            $.ajax({
                url: $("[data-list-modal-url]").val(),
                method: "POST",
                dataType: "html",
                data: {
                    "type": type,
                    "id": id
                }
            }).success(function (result) {
                result = JSON.parse(result);
                $('[data-modal]').empty();
                $('[data-modal]').html(result.data);
                $('#listmodal').modal('show');
            });
        }
    }

    var dashboard = function () {
        $("div.bhoechie-tab-menu>div.list-group>a").click(function (e) {
            e.preventDefault();
            $(this).siblings('a.active').removeClass("active");
            $(this).addClass("active");
            var index = $(this).index();
            $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
            $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
        });
    }

    return {
        init: function () {
            domainSearch();
            dashboard();
        }

    };

}();

$(function () {
    Home.init();
});