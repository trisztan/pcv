##PCV HOSTING SCRIPT

Pcv Hosting Script is a full featured billing app with cpanel & billingo & barion integration

##Features
- Laravel 5.3
- nodeJs
- jQuery
- Laravel Mix

##Base Html

http://crazycafe.net/demos/hostlab/

##Packages

https://github.com/spatie/laravel-permission
https://packagist.org/packages/laravelhungary/laravel-barion
https://github.com/Crinsane/LaravelShoppingcart

##Setup Project
composer install
npm install --no-bin-links

##Usefull commands
npm run dev
npm run production
npm run watch
