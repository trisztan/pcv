<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForeignKeys extends Migration
{
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->foreign('user_id')
                   ->references('id')
                   ->on('users')
                   ->onDelete('cascade');
        });

        Schema::table('order_item', function (Blueprint $table) {
            $table->foreign('order_id')
                   ->references('id')
                   ->on('orders')
                   ->onDelete('cascade');

            $table->foreign('item_id')
                   ->references('id')
                   ->on('items')
                   ->onDelete('cascade');
        });

        Schema::table('item_feature', function (Blueprint $table) {
            $table->foreign('item_id')
                   ->references('id')
                   ->on('items')
                   ->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('orders', function (Blueprint $table) {
            $table->dropForeign('orders_user_id_foreign');
        });
        Schema::table('order_item', function (Blueprint $table) {
            $table->dropForeign('order_item_order_id_foreign');
        });
        Schema::table('item_feature', function (Blueprint $table) {
            $table->dropForeign('item_feature_item_id_foreign');
        });
        Schema::enableForeignKeyConstraints();
    }
}
