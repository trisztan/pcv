<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

/* Default login */
Route::group(['middleware' => ['web']], function () {

    /* Pages */
    Route::get('/', 'HomeController@index');

    Route::get('megrendeles', 'HomeController@checkout');
    Route::post('checkout', 'HomeController@checkout');

    Route::get('domain-kereso', 'HomeController@indexDomain');

    /* Login */
    Route::auth();
    Route::get('logout', 'Auth\LoginController@logout');

    /* Domain search */
    Route::post('domainSearch', 'DomainController@domainSearch');

    /* Manage cart */
    Route::post('cart/add', 'CartController@cartAdd');
    Route::post('cart/remove', 'CartController@cartRemove');

    Route::get('arajanlat', 'HomeController@website');
});

Route::group(['middleware' => ['web', 'auth']], function () {
    Route::get('ugyfeladmin', 'DashboardController@index');

    Route::get('hibajegy', 'TicketController@create');
    Route::post('hibajegy', 'TicketController@store');
});

/* Route all missing route to 404 */

Route::any('{all}', function () {
    return view('errors.404');
})->where('all', '.*');
