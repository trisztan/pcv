<?php

namespace App\Pcv\Seeds;

use App\Pcv\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->email = 'baloghferenclevente@gmail.com';
        $user->firstname = 'Ferenc';
        $user->lastname = 'Balogh';
        $user->company = 'Balogh';
        $user->country = 'Magyarország';
        $user->city = 'Debrecen';
        $user->zipcode = '4031';
        $user->street = 'Kunhalom';
        $user->street_type = 'Kunhalom';
        $user->hsz = '8';
        $user->phone = '+36 30 473 5776';
        $user->password = bcrypt('123456');
        $user->save();

        $user = new User();
        $user->email = '22@gmail.com';
        $user->firstname = 'Ferenc';
        $user->lastname = 'Balogh';
        $user->company = 'Balogh';
        $user->country = 'Magyarország';
        $user->city = 'Debrecen';
        $user->zipcode = '4031';
        $user->street = 'Kunhalom';
        $user->street_type = 'Kunhalom';
        $user->hsz = '8';
        $user->phone = '+36 30 473 5776';
        $user->password = bcrypt('123');
        $user->save();
    }
}
