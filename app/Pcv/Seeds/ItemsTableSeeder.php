<?php

namespace App\Pcv\Seeds;

use App\Pcv\Models\Item;
use App\Pcv\Models\ItemFeature;
use Illuminate\Database\Seeder;

class ItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $domains[] = ['.hu', '2500', '2 évre történik meg a bejegyzés'];
        $domains[] = ['.com', '2700', ''];
        $domains[] = ['.net', '3100', ''];
        $domains[] = ['.org', '2900', ''];
        $domains[] = ['.eu', '2500', ''];
        $domains[] = ['.info', '2700', ''];
        $domains[] = ['.biz', '3490', ''];
        $domains[] = ['.asia', '5500', ''];
        $domains[] = ['.me', '6900', ''];
        $domains[] = ['.ro', '14900', ''];
        $domains[] = ['.de', '4900', '.DE domain esetén kötelező megadni egy németországi kapcsolattartót.'];
        $domains[] = ['.at', '4900', 'Az osztrák szabályok alapján 11 hónapra szól a regisztráció'];
        $domains[] = ['.dk', '4900', ''];
        $domains[] = ['.hr', '44900', ''];
        $domains[] = ['.ru', '5900', ''];
        $domains[] = ['.se', '7700', '.SE esetén szükséges a személyi igazolványszám (magánszemély esetén) vagy adószám (cég esetén).'];
        $domains[] = ['.si', '7700', ''];
        $domains[] = ['.ch', '4900', ''];
        $domains[] = ['.es', '4900', ''];
        $domains[] = ['.co.uk', '3900', ''];
        $domains[] = ['.gr', '4900', ''];
        $domains[] = ['.cn', '14900', '.CN esetén személyi igazolvány vagy útlevél másolatát kell elküldeni'];
        $domains[] = ['.tw', '22200', ''];
        $domains[] = ['.bg', '17700', 'Igénylőlap kitöltése szükséges (ezt a rendelés után elküldjük Önnek).'];
        $domains[] = ['.com.al', '33300', ''];
        $domains[] = ['.net.al', '33300', ''];
        $domains[] = ['.org.al', '33300', ''];
        $domains[] = ['.edu.al', '33300', ''];
        $domains[] = ['.co.at', '13900', ''];
        $domains[] = ['.or.at', '11100', ''];
        $domains[] = ['.be', '3900', ''];
        $domains[] = ['.cy', '11900', ''];
        $domains[] = ['.by', '9900', ''];
        $domains[] = ['.cz', '4900', ''];
        $domains[] = ['.com.de', '5500', ''];
        $domains[] = ['.ee', '13900', '.EE esetén személyi szám vagy adószám megadása szükséges, és kötelező megadni egy észtországi kapcsolattartót is.'];
        $domains[] = ['.co.ee', '13900', '.CO.EE esetén személyi szám vagy adószám megadása szükséges, és kötelező megadni egy észtországi kapcsolattartót is.'];
        $domains[] = ['.com.es', '18900', ''];
        $domains[] = ['.org.es', '18900', ''];
        $domains[] = ['.gl', '17700', ''];
        $domains[] = ['.com.hr', '9900', ''];
        $domains[] = ['.hu.com', '26600', ''];
        $domains[] = ['.is', '19900', ''];
        $domains[] = ['.it', '4900', '.IT domaint csak olasz cég / magánszemély regisztrálhat'];
        $domains[] = ['.co.je', '29900', ''];
        $domains[] = ['.je', '26600', ''];
        $domains[] = ['.li', '4900', ''];
        $domains[] = ['.lt', '4900', ''];
        $domains[] = ['.lu', '9900', ''];
        $domains[] = ['.lv', '9900', ''];
        $domains[] = ['.nl', '4900', ''];
        $domains[] = ['.no', '4900', '.NO domaint csak norvég cég / magánszemély regisztrálhat'];
        $domains[] = ['.no.com', '27700', ''];
        $domains[] = ['.biz.pl', '27700', ''];
        $domains[] = ['.com.pl', '27700', ''];
        $domains[] = ['.info.pl', '27700', ''];
        $domains[] = ['.net.pl', '27700', ''];
        $domains[] = ['.org.pl', '27700', ''];
        $domains[] = ['.pl', '4900', ''];
        $domains[] = ['.pt', '9900', ''];
        $domains[] = ['.com.pt', '55500', ''];
        $domains[] = ['.com.ro', '16600', ''];
        $domains[] = ['.ru.com', '26600', ''];
        $domains[] = ['.se.com', '26600', ''];
        $domains[] = ['.se.net', '26600', ''];
        $domains[] = ['.rs', '17700', '.RS esetén személyi szám vagy adószámot kell megadni, és kötelező megadni egy szerbiai kapcsolattartó'];
        $domains[] = ['.ua', '19900', '.UA esetén védjegy számra lesz szükségünk.'];
        $domains[] = ['.com.ua', '4900', ''];
        $domains[] = ['.gb.com', '26600', ''];
        $domains[] = ['.me.uk', '6600', ''];
        $domains[] = ['.org.uk', '6600', ''];
        $domains[] = ['.ag', '66600', ''];
        $domains[] = ['.com.ag', '44400', ''];
        $domains[] = ['.bz', '15000', ''];
        $domains[] = ['.ca', '29900', ''];
        $domains[] = ['.cl', '55500', ''];
        $domains[] = ['.com.co', '77700', ''];
        $domains[] = ['.co.cr', '33300', ''];
        $domains[] = ['.dm', '77700', ''];
        $domains[] = ['.com.do', '77700', ''];
        $domains[] = ['.com.ec', '66600', ''];
        $domains[] = ['.gs', '22200', ''];
        $domains[] = ['.com.jm', '36600', ''];
        $domains[] = ['.ky', '66600', ''];
        $domains[] = ['.vg', '22200', ''];
        $domains[] = ['.com.pe', '66600', ''];
        $domains[] = ['.ph', '77700', ''];
        $domains[] = ['.com.pa', '36600', ''];
        $domains[] = ['.com.pr', '77700', ''];
        $domains[] = ['.com.py', '55500', ''];
        $domains[] = ['.qc.com', '26600', ''];
        $domains[] = ['.tc', '22200', ''];
        $domains[] = ['.us', '3300', ''];
        $domains[] = ['.us.com', '26600', ''];
        $domains[] = ['.com.uy', '66600', ''];
        $domains[] = ['.uy.com', '25500', ''];
        $domains[] = ['.com.ve', '55500', ''];
        $domains[] = ['.ms', '22200', ''];
        $domains[] = ['.dj', '77700', ''];
        $domains[] = ['.hm', '26600', ''];
        $domains[] = ['.ma', '77700', ''];
        $domains[] = ['.sh', '33300', ''];
        $domains[] = ['.com.sc', '35900', ''];
        $domains[] = ['.net.sc', '35900', ''];
        $domains[] = ['.org.sc', '35900', ''];
        $domains[] = ['.sc', '35900', ''];
        $domains[] = ['.co.za', '77700', ''];
        $domains[] = ['.za.com', '26600', ''];
        $domains[] = ['.ae', '77700', ''];
        $domains[] = ['.am', '27700', ''];
        $domains[] = ['.cc', '7700', ''];
        $domains[] = ['.cn.com', '8800', ''];
        $domains[] = ['.com.cn', '4400', ''];
        $domains[] = ['.net.cn', '4400', ''];
        $domains[] = ['.org.cn', '4400', ''];
        $domains[] = ['.co.id', '88800', ''];
        $domains[] = ['.co.il', '33300', ''];
        $domains[] = ['.firm.in', '5500', ''];
        $domains[] = ['.gen.in', '5500', ''];
        $domains[] = ['.ind.in', '5500', ''];
        $domains[] = ['.in', '5500', ''];
        $domains[] = ['.net.in', '5500', ''];
        $domains[] = ['.org.in', '5500', ''];
        $domains[] = ['.jp', '39900', '.JP domaint csak japán cég vagy magányszemély regisztrálhat.'];
        $domains[] = ['.jpn.com', '26600', ''];
        $domains[] = ['.kg', '77700', ''];
        $domains[] = ['.co.kr', '133300', ''];
        $domains[] = ['.kz', '69900', ''];
        $domains[] = ['.la', '16600', ''];
        $domains[] = ['.mn', '66600', ''];
        $domains[] = ['.com.my', '77700', ''];
        $domains[] = ['.com.pk', '55500', ''];
        $domains[] = ['.sa.com', '26600', ''];
        $domains[] = ['.com.sg', '26000', '.COM.SG domaint csak szingapúri cég vagy magánszemély regisztrálhat, és szükség van adószámra vagy személyiigazolvány-számra.'];
        $domains[] = ['.sg', '26600', '.SG domaint csak szingapúri cég vagy magánszemély regisztrálhat, és szükség van adószámra vagy személyiigazolvány-számra.'];
        $domains[] = ['.tj', '77700', ''];
        $domains[] = ['.tm', '33300', ''];
        $domains[] = ['.com.tw', '22200', ''];
        $domains[] = ['.idv.tw', '22200', ''];
        $domains[] = ['.org.tw', '22200', ''];
        $domains[] = ['.com.vn', '77700', ''];

        $domains[] = ['.com.vn', '77700', ''];
        $domains[] = ['.com.vn', '77700', ''];
        $domains[] = ['.com.vn', '77700', ''];
        $domains[] = ['.com.vn', '77700', ''];

        foreach ($domains as $domain) {
            $item = new Item;
            $item->name = $domain[0];
            $item->type = 'domain';
            $item->price = round(($domain[1] * 1.27), -2);
            $item->description = $domain[2];
            $item->save();
        }

        /* Webspaces */
        $spaces[1]['item'] = ['Személyes', '7900', '4900', 'Népszerű'];
        $spaces[1]['feature']['size'] = '200 MB';
        $spaces[1]['feature']['database'] = 1;
        $spaces[1]['feature']['addon_domain'] = 1;
        $spaces[1]['feature']['email'] = 10;

        $spaces[2]['item'] = ['Közepes', '9900', '6900', 'Akciós'];
        $spaces[2]['feature']['size'] = '500 MB';
        $spaces[2]['feature']['database'] = 3;
        $spaces[2]['feature']['addon_domain'] = 1;
        $spaces[2]['feature']['email'] = 10;

        $spaces[3]['item'] = ['Profi', '12900', '9900', 'Akciós'];
        $spaces[3]['feature']['size'] = '1000 MB';
        $spaces[3]['feature']['database'] = 3;
        $spaces[3]['feature']['addon_domain'] = 1;
        $spaces[3]['feature']['email'] = 10;

        $spaces[4]['item'] = ['Üzleti', '14990', '13900', 'Akciós'];
        $spaces[4]['feature']['size'] = '3000 MB';
        $spaces[4]['feature']['database'] = 3;
        $spaces[4]['feature']['addon_domain'] = 3;
        $spaces[4]['feature']['email'] = 50;

        $spaces[5]['item'] = ['Vállalati', '18900', '16900', 'Akciós'];
        $spaces[5]['feature']['size'] = '5000 MB';
        $spaces[5]['feature']['database'] = 5;
        $spaces[5]['feature']['addon_domain'] = 5;
        $spaces[5]['feature']['email'] = 50;

        /* Features */
        $features[] = ['name' => 'size', 'title' => 'Tárhely mérete', 'value' => '200 MB', 'description' => 'Megjegyzés'];
        $features[] = ['name' => 'ssl', 'title' => 'Ingyenes SSL', 'value' => 'yes', 'description' => 'Megjegyzés'];
        $features[] = ['name' => 'admin', 'title' => 'Cpanel admin felület', 'value' => 'yes', 'description' => 'Megjegyzés'];
        $features[] = ['name' => 'backup', 'title' => 'Biztonsági mentés', 'value' => 'Napi, Heti, Havi', 'description' => 'Megjegyzés'];
        $features[] = ['name' => 'database', 'title' => 'MySQL/PostgreSQL', 'value' => '1 db', 'description' => 'Megjegyzés'];
        $features[] = ['name' => 'email', 'title' => 'E-mail címek', 'value' => '10 db', 'description' => 'Megjegyzés'];
        $features[] = ['name' => 'cms', 'title' => 'CMS barát', 'value' => 'yes', 'description' => 'Megjegyzés'];
        $features[] = ['name' => 'addon_domain', 'title' => 'Külön domainek', 'value' => '1 db', 'description' => 'Megjegyzés'];
        $features[] = ['name' => 'sla', 'title' => 'Rendelkezésre állás', 'value' => '99,99 %', 'description' => 'Megjegyzés'];

        foreach ($spaces as $space) {
            $item = new Item;
            $item->type = 'webspace';
            $item->name = $space['item'][0];
            $item->price = $space['item'][1];
            $item->discount = $space['item'][2];
            $item->discount_text = $space['item'][3];
            $item->discount_active = 1;
            $item->save();

            foreach ($features as $feature) {
                $feat = new ItemFeature;
                $feat->name = $feature['name'];
                $feat->title = $feature['title'];
                $feat->value = isset($space['feature'][$feature['name']]) ? $space['feature'][$feature['name']] : $feature['value'];
                $feat->item_id = $item->id;
                $feat->save();
            }
        }
    }
}
