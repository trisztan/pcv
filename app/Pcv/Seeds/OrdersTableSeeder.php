<?php

namespace App\Pcv\Seeds;

use App\Pcv\Models\Order;
use Illuminate\Database\Seeder;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $order = new Order();
        $order->code = str_random(10);
        $order->status = 'paid';
        $order->user_id = 1;
    }
}
