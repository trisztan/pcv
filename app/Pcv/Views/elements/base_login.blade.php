<!-- ** start login area **  -->
    <div class="loginArea sp90 bg-ptrn animated">
        <div class="container">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <form action="{{ url('/login') }}" class="loginForm" method="post">
                    {{ csrf_field() }}


                    <div class="regFormDt">
                        <div class="inputGroup">
                            <input type="email" name="email" value="{{ old('email') }}" placeholder="E-mail cím">
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                            <input name="password" type="password" placeholder="Jelszó">
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <div class="col-md-6">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember"> Megjegyzés
                                    </label>
                                </div>
                            </div>
                        </div>
                        <button type="submit" name="button">Bejelentkezés</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<!-- ** end login area **  -->