<ul class="nav navbar-nav navbar-right nav-cart">
    <li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle-cart data-toggle="dropdown" role="button" aria-expanded="false"> <i class="fa fa-2x green fa-shopping-cart"></i> Kosár</a>
        <ul class="dropdown-menu dropdown-cart" role="menu">
            <form>
            @if(Cart::count() > 0)
            @foreach(Cart::content() as $cart)
            <li>
                <span class="item">
                    <span class="item-left">
                        <span class="item-info">
                            <span>({{$cart->qty}} db) {{$cart->name}}</span>
                            <span><b>{{$cart->price}} Ft</b></span>
                        </span>
                    </span>
                    <span class="item-right">
                        <button class="btn btn-xs btn-danger pull-right" data-cart-remove="{{ $cart->rowId }}">x</button>
                    </span>
                </span>
            </li>

            @endforeach
            <li class="text-center">Összesen: <b>{{Cart::subtotal(0)}} Ft</b></li>
            <li class="checkout"><a class="btn btn-blue btn-block text-center" href="{{url('megrendeles')}}"><i class="fa fa-credit-card"></i> Megrendelés</a></li>
            @else
            <li class="text-center"><b>A kosár üres.</b></li>
            @endif
            </form>
        </ul>
    </li>
</ul>