<div class="homeArea"   id="particles-js">  <!-- ** start homeArea **  -->
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-5">
                <div class="homeImg"><img src="img/01.png" alt=""></div>
            </div>
            <div class="col-lg-6 col-md-7">
                <div class="homeSlider">
                    <div class="singleHomeSlide">
                        <span class="topTxt">Modern &nbsp;  - &nbsp;  Biztonságos &nbsp; - &nbsp;  Megbízható</span>
                        <span class="h1"><span class="count">634</span> db kiadott tárhely <br><span class="count">312</span> db domaint üzemeltetünk</span>
                        <p>Ismerje meg tárhelyeink részletes tulajdonságait, amennyiben kérdése van kattintson a kapcsolat gombra.</p>
                        <div class="homeBtns">
                            <a href="#" class="Btn homeBtn">Tárhely csomagjaink</a>
                            <a href="#" class="Btn homeBtn">Kapcsolat</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>  <!-- ** end homeArea **  -->