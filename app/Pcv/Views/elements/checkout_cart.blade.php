<div class="smallCartTotalWrep">
    <div class="head">
        <div class="h3">Kosár tartalma</div>    
    </div>
    <span>A feltüntett árak bruttó árak</span>
    <ul class="smallCartTotal">
        <li>
            <span class="product">Szolgáltatás</span>
            <span class="price">Ár</span>
        </li>
        @foreach(Cart::content() as $cart)
        <li>
            <span class="product">({{$cart->qty}} db) {{$cart->name}}</span>
            <span class="price">{{$cart->price}} Ft</span>
            <span class="button"><button type="button" class="btn btn-xs btn-danger pull-right" data-cart-remove="{{ $cart->rowId }}">x</button></span>
        </li>
        @endforeach
        <li>
            <span class="product">Összesen fizetendő</span>
            <span class="price">{{ Cart::subtotal(0) }} Ft</span>
        </li>
    </ul>
</div>