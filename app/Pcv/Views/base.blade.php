<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- /CSRF Token -->
    <link rel="icon" href="{{ asset('favicon.ico') }}" type="image/png">
    <title>PCV - Domain és Tárhely szolgáltatás</title>

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,600,300,300italic,600italic,700,700italic">
    {{--<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/icofont.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.css') }}">
    <link rel="stylesheet" href="{{ asset('css/animate.css') }}">--}}
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    <!-- custom -->
    {{--<link class="mainCss" rel="stylesheet" href="{{ asset('css/custom/style.css') }}">--}}
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script>
        var URL_BASE = {!! json_encode(url('/')) !!};
        var URL_DOMAINSEARCH = {!! json_encode(url('domainSearch')) !!};
        var URL_CARTADD = {!! json_encode(url('cart/add')) !!};
        var URL_CARTREMOVE = {!! json_encode(url('cart/remove')) !!};
        var URL_CHECKOUT = {!! json_encode(url('megrendeles')) !!};
    </script>
</head>

<body class="home">
<!-- FACEBOOK -->
<div id="fb-root"></div>
<script>
 window.fbAsyncInit = function() {
    FB.init({
      appId      : '713770292020220',
      xfbml      : true,
      version    : 'v2.8'
    });
    FB.AppEvents.logPageView();
    FB.Event.subscribe('edge.create', function(href, widget) {
       /*alert('cica');*/
    });
  };

(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/hu_HU/sdk.js#xfbml=1&version=v2.8&appId=713770292020220";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

</script>
<!-- /FACEBOOK -->

<div class="mainWrap">

<!-- ** start HeroArea **  -->
    <div class="heroArea">
        <header> <!-- ** start homeArea **  -->
            <div class="headerTop">  <!-- ** start headerTop **  -->
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-xs-4">
                            <ul class="topInfo">
                                {{--<li class="langOpt"><a href="#">Magyar</a></li>--}}
                                <li class="topLink "><a href="tel:+(36)304725776">+(36) 304725776</a></li>
                                <li class="topLink"><a href="mailto:support@pcv.hu">support@pcv.hu</a></li>
                            </ul>
                        </div>
                        <div class="col-md-6 col-xs-8">
                            <div class="topRight clearfix">
                                <ul class="signUpWrep fR">
                                    @if(Auth::check())
                                    <li><a href="{{ url('ugyfeladmin') }}">Üdv, {{Auth::user()->firstname}}</a></li>
                                    <li><a href="{{ url('logout') }}"><i class="fa fa-times red"></i> Kilépés</a></li>
                                    @else 
                                    <li><a href="{{ url('ugyfeladmin') }}">Bejelentkezés</a></li>
                                    <li><a href="{{ url('regisztracio') }}">Regisztráció</a></li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="headerBtm">  <!-- ** start headerBottom **  -->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-3 col-xs-6">
                            <a href="{{ url('/') }}" class="logo"><img src="img/logo.png" alt=""></a>
                        </div>
                        <div class="col-sm-9 col-xs-6">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                    <span class="sr-only"></span>
                                    <i class="fa fa-navicon"></i>
                                </button>
                            </div>
                            <nav id="navbar" class="navbar-collapse collapse">
                                <ul class="nav navbar-nav">
                                    <li><a href="#webspaces">Tárhely csomagjaink</a>
                                        {{--<ul class="dropdown-menu">
                                            <li><a href="sharedHosting.html">Wordpress tárhely</a></li>
                                            <li><a href="cloudHosting.html">Joomla tárhely</a></li>
                                            <li><a href="vpnHosting.html">Druplal tárhely</a></li>
                                            <li><a href="resellerHosting.html">Laravel tárhely</a></li>
                                        </ul>--}}
                                    </li>
                                    <li class="current-menu-item"><a href="#domains">Domain kereső</a>
                                    </li>
                                    {{--<li><a href="#">VPS bérlés</a></li>--}}
                                    {{--<li><a href="{{ url('blog') }}">Blog</a></li>--}}

                                    <li class="admin"><a href="@if(Auth::check()){{ url('ugyfeladmin') }}@else{{ url('login') }} @endif">Ügyféladmin</a>
                                    </li>
                                </ul>
                                @include('elements.base_cart')
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </header>  <!-- ** end header **  -->

        @yield('slider');
    </div>
<!-- ** end heroArea **  -->

@yield('content')

<!-- ** start Footer **  -->
    <footer class="spt90 footer">
        <div class="footerTop animated">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="widget footer_widget big">
                            <a href="index-2.html" class="footerLogo"><img src="img/footerLogo.png" alt=""></a>
                            <p>A PCV egy professzionális domain és tárhely szolgáltató. Specializálva magánszemélyek, vállalkozások számára, minden ügyfelünknek garantált 99,9%-os rendelkezésre állást biztosítunk.</p>
                        </div>
                    </div>
                    <div class="col-md-3 pull-right col-xs-6">
                        <div class="widget footer_widget big">
                            <div class="widget_title">Hírlevél</div>
                            <p>Íratkozzon fel hírlevelünkre, hogy hasznos információk birtokába jutassok.</p>
                            <div class="newsletterInput">
                                <input type="email" placeholder="Az ön e-mail címe">
                                <input type="submit" value="&#xf0c5;">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-6">
                        <div class="widget footer_widget small">
                            <div class="widget_title">Szolgáltatásaink</div>
                            <ul>
                                <li><a href="sharedHosting.html">Tárhely csomagok</a></li>
                                <li><a href="cloudHosting.html">Domain regisztráció</a></li>
                                <li><a href="dedicatedHosting.html">Virtuális szerver</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-6">
                        <div class="widget footer_widget small">
                            <div class="widget_title">Hasznos linkek</div>
                            <ul>
                                <li><a href="#">Domain kereső</a></li>
                                <li><a href="#">Ügyfélszolgálat</a></li>
                                <li><a href="#">Ügyféladmin</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-6">
                        <div class="widget footer_widget small">
                            <div class="widget_title">Ügyfélszolgálat</div>
                            <ul>
                                <li><a href="#">Kapcsolat</a></li>
                                <li><a href="#">Blog</a></li>
                                <li><a href="#">ÁSZF</a></li>
                                <li><a href="#">Adatvédelem</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footerBtm animated">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="footerTxt">
                            <p>Copyright 2016 - Minden jog fenntartva</p>
                            <p>Készítette &nbsp; <span>&#xecfd;</span>  &nbsp; Balogh Ferenc</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
<!-- ** end Footer **  -->

<div id="particles-js" style="display:none"></div>

</div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ mix('js/app.js') }}"></script>
    <script id="__bs_script__">//<![CDATA[
        document.write("<script async src='http://HOST:3200/browser-sync/browser-sync-client.js?v=2.18.8'><\/script>".replace("HOST", location.hostname));
    //]]></script>

    <!--Start of Tawk.to Script-->
        <script type="text/javascript">
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/585f1485f81f1673b64457dc/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
        })();
        </script>
    <!--End of Tawk.to Script-->

</body>
</html>
