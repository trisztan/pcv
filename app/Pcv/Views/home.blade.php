@extends('base')

@section('slider')
    @include('elements.home_slider')
@stop

@section('content')
<div class="mainWrap" id="domains">
 <!-- ** start domainSearchArea **  -->
    <div  class="domainSearchArea">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="doSearchW">
                        <div class="h3 searchTitle">Rendelje meg nálunk domain címét!</p></div>
                        <form class="domainSearch">
                            <div class="inputTop">
                                <input type="search" data-domain-address value="" placeholder="Írja be a domain címét">
                                <input type="button" data-domain-search value="&#xedef;">
                            </div>
                        </form>
                    </div>
                    <div class="alert hide mt-10 col-md-8 col-md-offset-2 animated fadeIn" data-domain-show></div>
                </div>
                <div class="col-md-6">
<div class="fb-page" data-href="https://www.facebook.com/pcvhu/" data-height="200" data-small-header="true" data-adapt-container-width="false" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/pcvhu/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/pcvhu/">PCV.HU</a></blockquote></div>
                </div>
            </div>
        </div>
    </div>
<!-- ** end domainSearchArea **  -->

<!-- ** start serviceArea **  -->
    <div class="serviceArea spt90 animated">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="sectionTitle">
                        <div class="h2">Miért minket választanak ügyfeleink?</div>
                        <span class="secTitleBar"></span>
                    </div>
                </div>
            </div>
            <div class="row serviceRow">
                <div class="col-md-3 col-sm-6 animated">
                    <div class="singleService">
                        <div class="serviceImg"><img src="img/icons/service01.png" alt=""></div>
                        <div class="serviceContent">
                            <div class="h5">Optimalizált tárhely</div>
                            <p>Tárhelyeink sok év (fejlesztői, rendszergazdai) tapasztalattal kerültek kialakításra.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 animated">
                    <div class="singleService">
                        <div class="serviceImg"><img src="img/icons/service02.png" alt=""></div>
                        <div class="serviceContent">
                            <div class="h5">Biztonsági mentés</div>
                            <p>Fejlett biztonsági mentés készítő rendszerünk 24 óránként menti adatait.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 animated">
                    <div class="singleService">
                        <div class="serviceImg"><img src="img/icons/service03.png" alt=""></div>
                        <div class="serviceContent">
                            <div class="h5">Ügyfélszolgálat</div>
                            <p>Célunk hogy minél többet segítsünk, ügyfélszolgálatunk a hét minden napján várja kérdéseit.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 animated">
                    <div class="singleService">
                        <div class="serviceImg"><img src="img/icons/service_05.png" alt=""></div>
                        <div class="serviceContent">
                            <div class="h5">CMS és php keretrendszerek</div>
                            <p>Tárhelyeink támogatják az összes népszerű cms-t (Wordpress) és php keretrendszert (CodeIngiter, Laravel)</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- ** end serviceArea **  -->
<!-- ** start cta **  -->
    <div class="cta animated">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="ctaContent">
                        <div class="h3 ctaTitle">Szakembereink az ön tárhelyét több mint 10 éves tapasztalattal üzemeltetik.</div>
                        {{--<a href="{{ url('tarhelyek')}}" class="ctaBtn Btn">Megtekintés</a>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- ** end cta **  -->
<!-- ** start pricingArea **  -->
    <div id="webspaces" class="pricingArea sp90 animated">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="sectionTitle">
                        <span class="h5">Válassza az ön számára legjobb tárhely csomagot</span>
                        <div class="h2">Tárhely csomagjaink összehasonlítása</div>
                        <span class="secTitleBar"></span>
                    </div>
                </div>
            </div>
            <div class="row priceRow">
                <div class="col-md-2 col-sm-6 animated">
                    <div class="packPrice">
                        <div class="priceHead">
                            <div class="h5">Tárhely adatok</div>
                        </div>
                        <ul class="priceContent">
                            @foreach($items[0]->features as $feature)
                            <li>{{$feature->title}}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                @foreach($items as $item)
                <div class="col-md-2 col-sm-6 animated">
                    <div class="singlePrice">
                        <div class="ribbon-box"><span>{{$item->discount_text}}</span></div>
                        <div class="priceHead">
                            <div class="h5">{{$item->name}}</div>
                        </div>
                        <ul class="priceContent">
                            @foreach($item->features as $feature)
                                @if($feature->value == 'yes')
                                <li><i class="fa fa-check green"></i></li>
                                @elseif($feature->value == 'no')
                                <li><i class="fa fa-times red"></i></li>
                                @else
                                <li>{{$feature->value}}</li>
                                @endif
                            @endforeach
                        </ul>
                        <div class="priceFoot">
                            @if($item->discount_active == 1)
                            <div class="price"><s>{{ $item->price }} Ft </s> <span>{{ $item->discount }} Ft</span></div>
                            @else
                            <div class="price"><span>{{ $item->price }} Ft</span></div>
                            @endif
                            1 évre
                            <button data-cart-add="{{ $item->name }} tárhely" data-cart-add-id="{{ $item->id }}" data-cart-add-type="webspace" class="priceBtn Btn ajax"><i class="fa fa-shopping-cart"></i> Kosárba</button>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
<!-- ** end pricingArea **  -->

<!-- ** start cta **  -->
    <div class="cta animated">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="ctaContent">
                        <div class="h3 ctaTitle">Tárhelyeink bővíthetőek. Az ön igényeinek megfelelően.</div>
                        {{--<a href="{{ url('tarhelyek')}}" class="ctaBtn Btn">Megtekintés</a>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- ** end cta **  -->

<!-- ** start testimonial-2 **  -->
    <div class="testimonialArea2 sptb90">
        <br/>
        <br/>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="sectionTitle mb45">
                        <span class="h5">Visszajelzések</span>
                        <div class="h2">elégedett ügyfeleinktől</div>
                        <span class="secTitleBar"></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="tstSlider2 owl-carousel">
                        <div class="singleSlide">
                            <div class="slideContent">
                                <p>A bérelt tárhely napi 3000 látogató és 11,000 db termék kiszolgálását hibamentesen végzi.</p>
                                <div class="clientDesc">
                                    <div class="h4">Kondoros Gábor</div>
                                    <span class="position">webshop tulajdonos</span>
                                </div>
                            </div>
                        </div>
                        <div class="singleSlide">
                            <div class="slideContent">
                                <p>Bemutatkozó honlapunknak a legkisebb csomagot választottuk, a 99,99%-os rendelkezésre állás miatt.</p>
                                <div class="clientDesc">
                                    <div class="h4">Kis Ádám</div>
                                    <span class="position">fodrász</span>
                                </div>
                            </div>
                        </div>
                        <div class="singleSlide">
                            <div class="slideContent">
                                <p>Személyes és ügyfeleim weboldalát futattom a tárhelyen. A tárhely nagy előnye a PHP váltási lehetőség</p>
                                <div class="clientDesc">
                                    <div class="h4">Tamás Zsolt</div>
                                    <span class="position">programozó</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                <br/>
        <br/>
    </div>
<!-- ** start testimonial-2 **  -->

<!-- ** start Newsletter **  -->
    {{--<div class="newsletterArea animated bg-ptrn">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="newsletter">
                        <div class="h3">SUBSCRIBE TO OUR NEWSLETTER</div>
                        <form action="#" class="nw">
                            <input type="email" placeholder="Your Email">
                            <input type="submit" value="subscribe">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>--}}
<!-- ** end Newsletter **  -->

@endsection
