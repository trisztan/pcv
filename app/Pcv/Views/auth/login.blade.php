@extends('base')

@section('slider')
<div class="pageTitleArea" id="particles-js">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="pageTitle">
                    <div class="h2">Bejelentkezés</div>
                    <span class="pageTitleBar"></span>
                    <br/>
                    <ul class="pageIndicate">
                        <li> Elfelejtetted a jelszavadat? <a href="{{ url('/password/reset') }}">Új kérése</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<!-- ** start login area **  -->
    <div class="loginArea sp90 bg-ptrn">
        <div class="container">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <form action="{{ url('/login') }}" class="loginForm" method="post">
                    {{ csrf_field() }}


                    <div class="regFormDt">
                        <div class="inputGroup">
                            <input type="email" name="email" value="{{ old('email') }}" placeholder="E-mail cím">
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                            <input name="password" type="password" placeholder="Jelszó">
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <div class="col-md-6">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember"> Megjegyzés
                                    </label>
                                </div>
                            </div>
                        </div>
                        <button type="submit" name="button">Bejelentkezés</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<!-- ** end login area **  -->
@endsection
