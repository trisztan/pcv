@extends('base') 
<!-- ** start chedkout **  -->
<!-- ** Start Cart Area **  -->

<!-- ** End Cart Area **  -->
@section('content')
<div class="checkoutArea sp10">
	<div class="container">
       {{ Form::open(array('url' => 'checkout')) }}
			<div class="col-md-12">
				<div class="checkTop">
					@if(Auth::check())
					<p>Üdv, {{ $user->firstname }}</p>
					@else
					<p>Van már fiókja? <a data-client-login href="#">Kattintson ide a bejelentkezéshez</a></p>
					@endif
				</div>
			</div>
            @if (!empty($error))
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($error as $err)
                            <li>{{ $err }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(Cart::count() > 0)
			<div class="col-md-6">
				<div>
					<div class="head">
						<div class="h3">Számlázási információk</div>
					</div>
					<div class="row">
						<div class="col-sm-6 singleInput">
							<input placeholder="Vezetéknév *" type="text" class="last_name" name="user[lastname]" value="@if(isset($user)){{ $user->lastname }} @endif" required autocomplete="given-name">
						</div>
                        <div class="col-sm-6 singleInput">
							<input placeholder="Keresztnév *" type="text" class="last_name" name="user[firstname]" value="@if(isset($user)){{ $user->firstname }} @endif" required autocomplete="additional-name">
						</div>
						<div class="col-sm-12 singleInput">
							<input placeholder="Cég" type="text" class="company_name" name="user[company]" value="@if(isset($user)){{ $user->company }} @endif" autocomplete="organization">
						</div>
						<div class="col-sm-6 singleInput">
							<input placeholder="Adószám *" type="taxnumber" class="taxnumber" name="user[taxnumber]" value="@if(isset($user)){{ $user->taxnumber }} @endif">
						</div>
						<div class="col-sm-6 singleInput">
							<input placeholder="E-mail cím *" type="email" class="email" name="user[email]" value="@if(isset($user)){{ $user->email }} @endif"  required autocomplete="email">
						</div>
                        @if(!isset($user))
                        <div class="col-sm-6 singleInput">
							<input placeholder="Jelszó *" type="text" class="password" name="user[password]" required>
						</div>
                        @endif
						<div class="col-sm-6 singleInput">
							<input placeholder="Telefonszám *" type="text" class="phone" name="user[phone]" value="@if(isset($user)){{ $user->phone }} @endif" required autocomplete="tel">
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6 singleInput select">
							<div class="singleSelect">
								<select name="user[country]">
                                <option value="Magyarország">Magyarország</option>
                            </select>
							</div>
						</div>
						<div class="col-sm-6 singleInput">
							<input placeholder="Irányítószám *" type="text" name="user[zipcode]" class="zipcode" required autocomplete="billing postal-code">
						</div>
						<div class="col-sm-12 singleInput">
							<input placeholder="Település *" type="text" name="user[city]" class="city"  required autocomplete="billing address-level2">
						</div>
						<div class="col-sm-6 singleInput">
							<input placeholder="Közterület jellege pl: utca,út *" type="text" name="user[street]" class="Irányítószám" required autocomplete="billing street-address">
						</div>
						<!--<div class="col-sm-12 singleInput">
							<div class="col-sm-2">
								<input placeholder="Hsz." type="text" class="hsz">
							</div>
							<div class="col-sm-2">
								<input placeholder="Ép." type="text" class="ep">
							</div>
							<div class="col-sm-2">
								<input placeholder="Lph." type="text" class="lph">
							</div>
							<div class="col-sm-2">
								<input placeholder="Szint" type="text" class="szint">
							</div>
                            <div class="col-sm-2">
								<input placeholder="Emelet" type="text" class="emelet">
							</div>
							<div class="col-sm-2">
								<input placeholder="Ajtó" type="text" class="ajto">
							</div>
						</div>-->
						<div class="col-sm-6 submitBtn">
							<input type="submit" value="Megrendelés">
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
                @include('elements.checkout_cart')
				<div class="paymentMethod">
					<div class="head">
						<div class="h3">Fizetési mód</div>
					</div>
					<div class="singleMethod">
						<div class="c_head">
							<div class="c_input">
								<input type="checkbox" id="checkOne" value="checkOne">
								<label for="checkOne"></label>
							</div>
							<div class="h5">Bankártyás</div>
						</div>
						<p>Ber tempor cum soluta nobis eleifend option congue ni imperdiet doming id quod.</p>
					</div>
					<div class="singleMethod">
						<div class="c_head">
							<div class="c_input">
								<input type="checkbox" id="checkTwo" value="checkTwo">
								<label for="checkTwo"></label>
							</div>
							<div class="h5">Paypal</div>
						</div>
						<img src="img/icons/paypal.png" alt="">
					</div>
				</div>
                30 napos pénzvisszafizetési garancia
			</div>
            @else
            
            @endif
		{{ Form::close() }}
	</div>
</div>
<!-- ** end chedkout **  -->
@endsection