@extends('base')

@section('slider')
<div class="pageTitleArea animated" id="particles-js">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="pageTitle">
                    <div class="h2">Domain kereső</div>
                    <span class="pageTitleBar"></span>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('content')
<!-- ** start domanArea **  -->
<div class="domainArea sp90 animated">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <form action="#" class="domainSearch">
                    <div class="inputTop">
                        <input type="search" placeholder="Írjon be egy domain címet">
                        <input type="submit" value="&#xedef;">
                    </div>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 col-md-offset-1 animated">
                <div class="domainResult">
                    <div class="resutlTop">
                        <div class="result hide"><span>hostlab.com</span> is available now!</div>
                        <div class="cartBtn">
                            <a href="#" class="addCart"><span class="added">Added To Cart</span><span class="add">Add To Cart</span></a>
                            <a href="#" class="buyNow">Megrendelés</a>
                        </div>
                    </div>
                    <ul class="resultTable animated">
                        <li class="tableHead">
                            <div class="singleDt domain">Domain</div>
                            <div class="singleDt duration">Év</div>
                            <div class="singleDt regPrice">Regisztráció</div>
                            <div class="singleDt trPrice">Átregisztráció</div>
                            <div class="singleDt rnPrice">Megújítás</div>
                        </li>
                        @foreach($domains as $domain)
                        <li class="tableDtc animated">
                            <div class="singleDt domain">{{ $domain->name }}</div>
                            <div class="singleDt duration">@if($domain->name == ".hu") 2 év @else 1 év @endif</div>
                            <div class="singleDt regPrice">{{ money($domain->price) }}</div>
                            <div class="singleDt trPrice">{{ money($domain->price) }}</div>
                            <div class="singleDt rnPrice">{{ money($domain->price) }}</div>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ** end domanArea **  -->

<!-- ** start Newsletter **  -->
    <div class="newsletterArea animated">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="newsletter">
                        <div class="h3">SUBSCRIBE TO OUR NEWSLETTER</div>
                        <form action="#" class="nw">
                            <input type="email" placeholder="Your Email">
                            <input type="submit" value="subscribe">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- ** end Newsletter **  -->
@endsection
