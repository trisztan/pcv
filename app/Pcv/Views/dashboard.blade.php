@extends('base')

@section('slider')
<div class="pageTitleArea animated" id="particles-js">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="pageTitle">
                    <div class="h2">Ügyféladmin</div>
                    <span class="pageTitleBar"></span>
                    <br/>
                    <ul class="pageIndicate">
                        <li>Számlázási ügyek, szolgáltatások kezelése, hosszabítása.</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="container">
	<div class="row">
        <div class="col-lg-12 col-md-5 col-sm-8 col-xs-9 bhoechie-tab-container">
            <div class="col-lg-2 col-md-3 col-sm-3 col-xs-3 bhoechie-tab-menu">
              <div class="list-group">
                <a href="#" class="list-group-item active text-center">
                  <i class="fa fa-cubes fa-2x" aria-hidden="true"></i><br/> Szolgáltatásaim
              </a>
              <a href="#" class="list-group-item text-center">
                  <i class="fa fa-home fa-2x" aria-hidden="true"></i><br/> Domainek
              </a>
              <a href="#" class="list-group-item text-center">
                  <i class="fa fa-user fa-2x" aria-hidden="true"></i><br/> Adatok
              </a>
              <a href="#" class="list-group-item text-center">
                  <i class="fa fa-file fa-2x" aria-hidden="true"></i><br/> Számlázás
              </a>
              <a href="#" class="list-group-item text-center">
                  <i class="fa fa-ticket fa-2x" aria-hidden="true"></i><br/> Hibajegyek
              </a>
          </div>
      </div>
      <div class="col-lg-10 col-md-9 col-sm-9 col-xs-9 bhoechie-tab">
        <!-- flight section -->
        <div class="bhoechie-tab-content active">
            @foreach($spaces as $space)
                @foreach($space->orders as $order)
                    <div class="panel panel-default">
                    <div class="panel-heading">Megrendelés azonosító: #{{ $order->code }}</div>
                    <div class="panel-body">
                        <div class="panel panel-default">
                            @foreach($order->orderItemsWithItem as $item)
                            <div class="panel-heading">{{ $item->name }} csomag</div>
                            <div class="panel-body">
                                <ul>
                                    <li>
                                        Megrendelés idejes: {{ $item->created_at }}
                                    </li>
                                    <li>
                                       Ár: {{ money($item->price) }} / év
                                    </li>
                                    <li>
                                        Állapotok: {{ $order->status }}
                                    </li>
                                </ul>
                            </div>
                  @endforeach
                </div>
                        </div>
                </div>
              @endforeach
      @endforeach
  </div>
  <!-- Domain section -->
  <div class="bhoechie-tab-content">
    <center>
      <h1 class="glyphicon glyphicon-road" style="font-size:12em;color:#55518a"></h1>
      <h2 style="margin-top: 0;color:#55518a">Cooming Soon</h2>
      <h3 style="margin-top: 0;color:#55518a">Train Reservation</h3>
  </center>
</div>

<!-- User section -->
<div class="bhoechie-tab-content">
    {{ $user }}
</div>
<div class="bhoechie-tab-content">
    {{ $invoice }}
</div>
</div>
</div>
</div>
</div>
</br>
@endsection