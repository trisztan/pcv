@extends('base')

@section('slider')
<div class="pageTitleArea animated" id="particles-js">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="pageTitle">
                    <div class="h2">Weblap árajánlat</div>
                    <span class="pageTitleBar"></span>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('content')
<!-- ** start domanArea **  -->
<div class="domainArea sp90 animated">
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <h2>1. lépés - Van már weboldala?</h2>
                <div class="checkbox">
                    <label><input type="radio" name="website" value="true"> Nem új weblapot szeretnék</label>
                </div>
                <div class="checkbox">
                    <label><input type="radio" name="website" value="false"> Már van weblapom és szeretném tovább fejleszteni</label>
                </div>

                <h2>2. lépés - Van már tárhelye vagy szervere?</h2>
                <div class="checkbox">
                    <label><input type="radio" name="website" value="false"> Igen, van tárhelyem</label>
                </div>
                 <div class="checkbox">
                    <label><input type="radio" name="website" value="false"> Nem, de szeretnék tárhelyet</label>
                </div>

                <h2>3. lépés - Rendelkezik domain címmel?</h2>
                <div class="checkbox">
                    <label><input type="radio" name="website" value="false"> Nem, de szeretnék domain címet</label>
                    <label><input type="radio" name="website" value="false"> Igen</label>
                </div>

                <h2>4. lépés - Milyen weblapot szeretne?</h2>
                    <div class="checkbox">
                        <label><input type="radio" name="website" value="false"> Céges bemutatkozó</label>
                    </div>
                    <div class="checkbox">
                        <label><input type="radio" name="website" value="false"> Webshop</label>                
                    </div>
                    <div class="checkbox">
                        <label><input type="radio" name="website" value="false"> Blog</label>                
                    </div>
                    <div class="checkbox">
                        <label><input type="radio" name="website" value="false"> Galéria</label>                
                    </div>
                    <div class="checkbox">
                        <label><input type="radio" name="website" value="false"> Fórum</label>
                    </div>
                    <li>
                    <li> Facebook megosztás</li>
                    <li> Webshop modul</li>
                    <li> Kapcsolat modul</li>
                    <li> Főoldal modul</li>
                    <li> Egy év tárhely + .hu domain regisztráció</li>
                    <li> Logó terv</li>
                    <li> Ingyenes sablon beállítás</li>
                    <li> Seo beállítások</li>
                    <li> Facebook megosztás</li>
                    <li> Alap seo beállítások</li>
                </ul>
            </div>
             <div class="col-md-6">
                 <h2>Rendelhető extrák</h2>
             </div>
        </div>
        <div class="row">
            <div class="col-md-10 col-md-offset-1 animated">
                <div class="domainResult">
                    <div class="resutlTop">
                        <div class="result hide"><span>hostlab.com</span> is available now!</div>
                        <div class="cartBtn">
                            <a href="#" class="addCart"><span class="added">Added To Cart</span><span class="add">Add To Cart</span></a>
                            <a href="#" class="buyNow">Megrendelés</a>
                        </div>
                    </div>
                    <ul class="resultTable animated">
                        <li class="tableHead">
                            <div class="singleDt domain">Domain</div>
                            <div class="singleDt duration">Év</div>
                            <div class="singleDt regPrice">Regisztráció</div>
                            <div class="singleDt trPrice">Átregisztráció</div>
                            <div class="singleDt rnPrice">Megújítás</div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ** end domanArea **  -->

<!-- ** start Newsletter **  -->
    <div class="newsletterArea animated">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="newsletter">
                        <div class="h3">SUBSCRIBE TO OUR NEWSLETTER</div>
                        <form action="#" class="nw">
                            <input type="email" placeholder="Your Email">
                            <input type="submit" value="subscribe">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- ** end Newsletter **  -->
@endsection
