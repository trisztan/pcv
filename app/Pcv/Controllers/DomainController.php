<?php

namespace App\Pcv\Controllers;

use Config;
use Helge\Loader\JsonLoader;
use Illuminate\Http\Request;
use Helge\Client\SimpleWhoisClient;
use Helge\Service\DomainAvailability;

class DomainController extends Controller
{
    public $json = [];

    /**
     * Search domain.
     *
     * @param Request $request
     * @param Config $config
     * @param SimpleWhoisClient $client
     * @return void
     */
    public function domainSearch(Request $request, Config $config, SimpleWhoisClient $client)
    {
        if (! empty($request->domain)) {
            $tlds = new JsonLoader(storage_path('app/servers.json'));
            $checkDomain = new DomainAvailability($client, $tlds);
            $domain = $request->domain;
            $domain = str_replace(['http://', 'www.', 'https://', '//:', '//'], '', $domain);

            if (substr_count($domain, '.') == 1) {
                $tld = explode('.', $domain);
                $tld = $tld[1];

                if (in_array(mb_strtoupper($tld), $config::get('tld'))) {
                    if ($checkDomain->isAvailable($domain)) {
                        $this->json['success'] = 'A domain cím szabad';

                        return response()->json($this->json);
                    }

                    $this->json['error'] = 'A domain cím használatban van';

                    return response()->json($this->json);
                }

                $this->json['error'] = 'A megadott domain cím végződés nem létezik';

                return response()->json($this->json);
            }

            $this->json['error'] = 'A domain cím formátuma hibás!';

            return response()->json($this->json);
        }

        $this->json['error'] = 'A domain cím megadása kötelező!';

        return response()->json($this->json);
    }
}
