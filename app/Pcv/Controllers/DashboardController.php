<?php

namespace App\Pcv\Controllers;

use Auth;
use App\Pcv\Models\User;
use App\Pcv\Models\Invoice;
use App\Pcv\Traits\CpanelTrait;

class DashboardController extends Controller
{
    use CpanelTrait;

    public function index(User $user, Invoice $invoice)
    {
        //return $this->getBandwidth('balpart');

        $spaces = $user->with('orders.orderItemsWithItem')->find(Auth::user()->id)->get();
        $user = $user->find(Auth::user()->id);
        $domains = $user->find(Auth::user()->id);
        $invoices = $invoice::where('user_id', Auth::user()->id)->get();

        return view('dashboard', compact('spaces', 'domains', 'user'));
    }
}
