<?php

namespace App\Pcv\Controllers;

use Cart;
use Barion;
use App\Pcv\Models\Item;
use App\Pcv\Models\User;
use App\Pcv\Models\Order;
use Illuminate\Http\Request;
use App\Pcv\Models\OrderItem;

class HomeController extends Controller
{
    public $json = [];

    /**
     * Show home page.
     *
     * @param Item $item
     * @return view
     */
    public function index(Item $item)
    {
        $items = $item::with('features')->webspace()->get();
        $domains = $item->domains()->get();

        return view('home', compact('items', 'domains'));
    }

    /**
     * Change session locale.
     * @param  Request $request
     * @return Response
     */
    public function changeLocale(Request $request)
    {
        $this->validate($request, ['locale' => 'required|in:fr,en']);

        Session::put('locale', $request->locale);

        return redirect()->back();
    }

    /**
     * Show domain search page.
     *
     * @param Item $item
     * @return view
     */
    public function indexDomain(Item $item)
    {
        $domains = $item->domains()->get();

        return view('search', compact('domains'));
    }

    /**
     * Show checkout page.
     *
     * @param Request $request
     * @param Cart $cart
     * @return view
     */
    public function checkout(Request $request, User $user, Order $order, OrderItem $orderItem, Cart $cart)
    {
        $error = [];

        if ($request->isMethod('post')) {
            if ($user->emailExist($request->user['email'])->count() == 1) {
                $error['email_used'] = 'Az e-mail cím használatban van.';
            } else {
                $newUser = $user::create($request->user);

                $newOrder = new $order;
                $newOrder->user_id = $newUser->id;
                $newOrder->status = 'new';
                $newOrder->save();

                $order::where('id', $newOrder->id)->update([
                    'code' => sprintf('%07d', $newOrder->id),
                ]);

                $items = [];
                foreach ($cart::content() as $item) {
                    $newOrderItem = new $orderItem;
                    $newOrderItem->order_id = $newOrder->id;
                    $newOrderItem->item_id = $item->options['id'];
                    $newOrderItem->price = (int) $item->price;
                    $newOrderItem->save();

                    $items[] = ['Name' => $item->name, 'Description' => $item->name, 'Quantity' => 1, 'Unit' => 'db', 'UnitPrice' => (int) $item->price, 'ItemTotal' => (int) $item->price];
                }

                $result = Barion::paymentStart([
                    'PaymentType' => 'Immediate',
                    'GuestCheckOut' => true,
                    'FundingSources' => ['All'],
                    'Locale' => 'hu',
                    'Currency' => 'HUF',
                    'Transactions' => [
                        [
                            'POSTransactionId' => 'PCV-'.sprintf('%07d', $newOrder->id),
                            'Payee' => $newUser->email,
                            'Total' => Cart::subtotal(),
                            'Items' => [$items],
                        ],
                    ],
                ]);

                return redirect($result->GatewayUrl);
            }
        }

        return view('checkout', compact('error'));
    }

    /**
     * [thankyou description].
     * @author trisztan baloghferenclevente@gmail.com
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function thankyou(Request $request)
    {
        return $request;
    }

    public function website(Request $request)
    {
        return view('website');
    }
}
