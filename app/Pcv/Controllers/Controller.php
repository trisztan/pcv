<?php

namespace App\Pcv\Controllers;

use Auth;
use View;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $user;

    /**
     * Global variables.
     */
    public function __construct()
    {
        // Set global user variable
        $this->user = Auth::user();
        View::share('user', $this->user);
    }
}
