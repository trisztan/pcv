<?php

namespace App\Pcv\Controllers;

use Cart;
/* Domain search */
use App\Pcv\Models\Item;
use Illuminate\Http\Request;

class CartController extends Controller
{
    public $json = [];

    /**
     * Add item to cart.
     *
     * @param Request $request
     * @param Cart $cart
     *
     * @return json
     */
    public function cartAdd(Request $request, Item $item, Cart $cart)
    {
        if ($request->type == 'domain') {
            $tld = explode('.', $request->item);
            $id = '.'.end($tld);
            $where['name'] = $id;
        } elseif ($request->type == 'webspace') {
            $id = $request->id;
            $where['id'] = $request->id;
        }

        $checkItem = $item::where($where)->where('type', $request->type);

        if ($checkItem->count() == 1) {
            $checkItem = $checkItem->first();
            $price = $checkItem->price_cart;
            $cart = $cart::add(str_random(10), $request->item, $request->year, $price, ['id' => $checkItem->id]);

            if ($cart) {
                $this->json['success'] = 'Sikeresen hozzáadva a kosárhoz.';
                $this->json['html'] = view('elements.base_cart')->render();

                return response()->json($this->json);
            }

            $this->json['error'] = 'Nem sikerült hozzáadni a kosárhoz.';

            return response()->json($this->json);
        }

        $this->json['error'] = 'Nem sikerült hozzáadni a kosárhoz.';

        return response()->json($this->json);
    }

    /**
     * Remove item from cart.
     *
     * @param Request $request
     * @param Cart $cart
     *
     * @return json
     */
    public function cartRemove(Request $request, Cart $cart)
    {
        // When fucking cart package return always null :|
        try {
            $cart::remove($request->id);
            $this->json['success'] = 'Sikeresen törölve.';
            $this->json['base_cart'] = view('elements.base_cart')->render();
            $this->json['checkout_cart'] = view('elements.checkout_cart')->render();

            return response()->json($this->json);
        } catch (\Exception $e) {
            $this->json['error'] = 'Nem sikerült törölni.';

            return response()->json($this->json);
        }
    }
}
