<?php
/**
 * Convert money for better.
 * @author Balogh Ferenc Levente <baloghferenclevente@gmail.com>
 * @date 2017-04-04
 *
 * @param int $number
 *
 * @return string
 */
function money($number)
{
    return number_format($number, 0, ',', '.').' Ft ';
}

/**
 * Add leading zeros to a number, if necessary.
 *
 * @var int The number to add leading zeros
 * @var int $threshold Threshold for adding leading zeros (number of digits
 *                     that will prevent the adding of additional zeros)
 * @return string
 */
function order_id($value, $threshold = 7)
{
    return sprintf('%0'.$threshold.'s', $value);
}

function pre($arr)
{
    echo '<pre>';
    print_r($arr);
    echo '</pre>';
}
