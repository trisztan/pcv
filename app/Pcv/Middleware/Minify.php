<?php

namespace App\Pcv\Middleware;

use Closure;

class Minify
{
    public $filters = [
        //Remove HTML comments except IE conditions
        '/<!--(?!\s*(?:\[if [^\]]+]|<!|>))(?:(?!-->).)*-->/s' => '',
        // Remove comments in the form /* */
        '/(?<!\S)\/\/\s*[^\r\n]*/' => '',
        // Shorten multiple white spaces
        '/\s{2,}/' => ' ',
        // Remove whitespaces between HTML tags
        '/>\s+</' => '><',
        // Collapse new lines
        '/(\r?\n)/' => '',
    ];

    /**
     * @param $request
     * @param Closure $next
     * @param int $cache
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        $html = $response->getContent();
        $html = preg_replace(array_keys($this->filters), array_values($this->filters), $html);

        return $response->setContent($html);
    }
}
