<?php

namespace App\Pcv\Traits;

use Cpanel;

trait CpanelTrait
{
    private $cpanel;
    private $api = 3;

    public function __construct()
    {
        $this->cpanel = new Cpanel([
            'host' => env('CPANEL_HOST'),
            'username' => env('CPANEL_USER'),
            'auth_type' => 'password',
            'password' => env('CPANEL_PASS'),
        ]);
    }

    public function getBandwidth($username)
    {
        return $this->cpanel->execute_action($this->api, 'Bandwidth', 'query', $username, ['grouping' => 'domain']);
    }

    public function listAccounts()
    {
        return $this->cpanel->listaccts();
    }
}
