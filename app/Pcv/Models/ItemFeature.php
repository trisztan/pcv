<?php

namespace App\Pcv\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemFeature extends Model
{
    use SoftDeletes;

    protected $table = 'item_feature';
    protected $dates = ['deleted_at'];
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /**
     * Item relation.
     *
     * @return object
     */
    public function item()
    {
        return $this->belongsTo('App\Pcv\Models\Item');
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
