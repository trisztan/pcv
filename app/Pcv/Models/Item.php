<?php

namespace App\Pcv\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

class Item extends Model
{
    use SoftDeletes;

    protected $appends = ['price_cart'];
    protected $dates = ['deleted_at'];
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /**
     * Item has many features.
     *
     * @return object
     */
    public function features()
    {
        return $this->hasMany('App\Pcv\Models\ItemFeature');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /**
     * Scope for webspace type.
     *
     * @param Builder $query
     * @return object
     */
    public function scopeWebspace(Builder $query)
    {
        return $query->where('type', 'webspace');
    }

    /**
     *  Scope for domain type.
     *
     * @param Builder $query
     * @return object
     */
    public function scopeDomains(Builder $query)
    {
        return $query->where('type', 'domain');
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /**
     * Get current price.
     *
     * @return int
     */
    public function getPriceCartAttribute()
    {
        return (int) $this->discount_active == 1 ? $this->discount : $this->price;
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
