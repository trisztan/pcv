<?php

namespace App\Pcv\Models;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    public $table = 'order_item';
    protected $dates = ['deleted_at'];

    /**
     * Order belongs to Order Item.
     *
     * @return object
     */
    public function order()
    {
        return $this->belongsTo('App\Pcv\Models\Order');
    }

    /**
     * Items relations.
     *
     * @return object
     */
    public function items()
    {
        return $this->hasOne('App\Pcv\Models\Item', 'id', 'item_id');
    }
}
