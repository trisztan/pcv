<?php

namespace App\Pcv\Models;

use Hash;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, HasRoles;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'firstname', 'lastname', 'country', 'city', 'zipcode', 'street', 'phone', 'email', 'password',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get the orders for the users.
     */
    public function orders()
    {
        return $this->hasMany('App\Pcv\Models\Order');
    }

    /**
     * Get the invoices for the users.
     */
    public function invoices()
    {
        return $this->hasMany('App\Pcv\Models\Invoice');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /**
     * Scope spaces.
     *
     * @param Builder $query
     * @return void
     */
    public function scopeSpaces(Builder $query)
    {
        return $query->where('user_id', 1);
    }

    /**
     * Check exist email.
     *
     * @param Builder $query
     * @param string $email
     * @return void
     */
    public function scopeEmailExist(Builder $query, $email)
    {
        return $query->where('email', $email);
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

    /**
     * Set password hash.
     *
     * @param string $password
     * @return void
     */
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = Hash::needsRehash($password) ? Hash::make($password) : $password;
    }
}
