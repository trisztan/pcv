<?php

namespace App\Pcv\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SoftDeletes;

    protected $table = 'orders';
    protected $dates = ['deleted_at'];

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /**
     * OrderItem relation.
     *
     * @return object
     */
    public function orderItems()
    {
        return $this->hasMany('App\Pcv\ModelsOrderItem');
    }

    /**
     * User relation.
     *
     * @return object
     */
    public function user()
    {
        return $this->belongsTo('App\Pcv\Models\User');
    }

    /**
     * Orders with items.
     *
     * @return object
     */
    public function orderItemsWithItem()
    {
        return $this->orderItems()->leftJoin('items', 'items.id', '=', 'order_item.item_id');
    }
}
