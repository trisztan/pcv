<?php

namespace App\Pcv\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invoice extends Model
{
    use SoftDeletes;

    protected $table = 'invoices';
    protected $dates = ['deleted_at'];
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /**
     * User relation.
     *
     * @return object
     */
    public function user()
    {
        return $this->belongsTo('App\Pcv\Models\User');
    }
}
