<?php

namespace App\Pcv\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['name'];

    public function tickets()
    {
        return $this->hasMany('App\Pcv\Models\Ticket');
    }
}
