<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(App\Pcv\Seeds\UsersTableSeeder::class);
        $this->call(App\Pcv\Seeds\ItemsTableSeeder::class);
        $this->call(App\Pcv\Seeds\OrdersTableSeeder::class);
    }
}
